<?php


function load_bootstrap() {
  // カスタマイズ版Bootstrap
  wp_enqueue_style( 'bootstrap_style', get_stylesheet_directory_uri() . '/assets/css/rtbs4.bootstrap.css', array(), '4.0.0' );
  wp_enqueue_script( 'bootstrap_script', get_template_directory_uri() . '/assets/bootstrap/dist/js/bootstrap.bundle.min.js', array('jquery'), '4.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'load_bootstrap' );


// 独自JSの読み込み
function load_my_script() {
  // 親テーマ
  //wp_enqueue_script( 'parent_script', get_template_directory_uri() . '/assets/js/scripts.js', array('jquery','bootstrap_script'), '', true );
  // 子テーマ
  wp_enqueue_script( 'child_script', get_stylesheet_directory_uri() . '/assets/js/scripts.js', array('jquery','bootstrap_script'), '', true );
}
add_action( 'wp_enqueue_scripts', 'load_my_script' );


// 独自CSSの読み込み
function load_my_style() {
  // 親テーマ
  wp_enqueue_style( 'parent_style', get_template_directory_uri() . '/style.css', array('bootstrap_style','fonts_google'), '' );
  // 子テーマ
  wp_enqueue_style( 'child_style', get_stylesheet_uri(), array('parent_style'), '' );
}
add_action( 'wp_enqueue_scripts', 'load_my_style' );


// ウェブフォントの読み込み
function load_fonts() {
  $fonts_google_args = array(
    'family' => '',
    'subset' => '',
  );
  wp_enqueue_style( 'fonts_google', add_query_arg( $fonts_google_args, 'https://fonts.googleapis.com/css' ), array(), '' );
}
add_action( 'wp_enqueue_scripts', 'load_fonts' );


// その他ライブラリの読み込み
function load_library() {
  // slick http://kenwheeler.github.io/slick/
  wp_enqueue_style( 'slick_css', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.css', array(), '' );
  wp_enqueue_style( 'slick_theme', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css', array('slick_css'), '' );
  wp_enqueue_script( 'slick_js', 'https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js', array('jquery'), '', true );
}
add_action( 'wp_enqueue_scripts', 'load_library' );
