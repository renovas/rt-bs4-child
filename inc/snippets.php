<?php
// Plugin Hook: Public Post Previewの公開有効期限を変更
function set_nonce_life() {
	$day = 30; // 30日間
	return 86400 * $day;
}
add_filter('ppp_nonce_life','set_nonce_life');
