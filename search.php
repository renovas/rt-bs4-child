<?php get_header(); ?>



<div id="content" class="site-content<?php if ( is_active_sidebar('sidebar-widget') ) { echo ' with-widget'; } ?>">
<div class="site-content-inner">



<div id="primary" class="content-area">
<main id="main" class="site-main">


<?php if ( have_posts() ) : ?>
  <header class="page-header">
    <h1 class="page-title h1 search-header">
      <?php
      if ( get_query_var( 'post_type' ) != null ) {
        $post_type = get_query_var( 'post_type' );
        //$post_type_obj = get_post_type_object( $post_type );
        //$post_type_name = $post_type_obj->labels->singular_name;
        switch ($post_type) {
          case 'post':
            $post_type_name = '投稿';
            break;
          case '投稿タイプのスラッグ':
            $post_type_name = '投稿タイプ名';
            break;
          case 'any':
            default:
            $post_type_name = 'サイト全体';
            break;
        }
      } else {
        $post_type_name = 'サイト全体';
      }

      echo '<div>検索結果［' . $post_type_name . 'から検索］</div>';
      echo '<div class="search-header-keyword">検索キーワード: ' . get_search_query() . '</div>';
      ?>
    </h1>
  </header><!-- .page-header -->

  <ul class="post-list">
    <?php while ( have_posts() ) : the_post(); ?>

    <?php if ( '投稿タイプのスラッグ' == $post->post_type ) : ?>
    <li class="row post-list-item">
      <div class="col-lg-12 text">
        <h2>
          <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </h2>
        <time class="time" datetime="<?php the_time('c'); ?>">
          <?php the_time('Y.m.d'); ?>
        </time>
        <span class="post-category">
          <?php echo esc_html(get_post_type_object($post->post_type)->label); ?>
        </span>
        <p>
          <?php /* echo wp_trim_words( get_the_content(), 80, '...' ); */ echo wp_trim_words( get_the_excerpt(), 80, '...' ); ?>
        </p>
      </div>
    </li><!-- / .post__list__item -->
    <?php else : ?>
    <li class="row post-list-item">
      <div class="col-lg-3 image">
        <a href="<?php the_permalink(); ?>">
          <?php the_post_thumbnail('small','class=img-fluid'); ?>
        </a>
      </div>

      <div class="col-lg-9 text">
        <h2>
          <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
        </h2>
        <time class="time" datetime="<?php the_time('c'); ?>">
          <?php the_time('Y.m.d'); ?>
        </time>
        <?php the_primary_category(); ?>
        <p>
          <?php /* echo wp_trim_words( get_the_content(), 80, '...' ); */ echo wp_trim_words( get_the_excerpt(), 80, '...' ); ?>
        </p>
      </div>
    </li><!-- / .post__list__item -->
    <?php endif; ?>

    <?php endwhile; ?>
  </ul><!-- / .post__list -->

  <?php get_template_part( 'template-parts/nav', 'archive' ); ?>
<?php else : ?>
  <?php get_template_part( 'template-parts/post/content', 'none' ); ?>
<?php endif; ?>


</main><!-- / .site-main -->
</div><!-- / .content-area -->



<?php get_sidebar(); ?>



</div><!-- / .site-content-inner -->
</div><!-- / .site-content -->



<?php get_footer(); ?>
