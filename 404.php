<?php get_header(); ?>



<div id="content" class="site-content<?php if ( is_active_sidebar('sidebar-widget') ) { echo ' with-widget'; } ?>">
<div class="site-content-inner">



<div id="primary" class="content-area">
<main id="main" class="site-main">


<section class="error-404 not-found">
  <header class="page-header">
    <h1 class="page-title h1">お探しのページは見つかりませんでした</h1>
  </header><!-- / .page-header -->

  <div class="page-content">
    <p>お手数をおかけして申し訳ございません。<br>お探しのページは、移動または削除された可能性があります。<br>トップページから再度お探しください。</p>

    <?php // get_search_form(); ?>

    <p>
      <a href="<?php echo home_url('/'); ?>" class="btn btn-primary">トップページに戻る</a>
    </p>
    <p>
      <a href="<?php echo site_url('/sitemap/'); ?>" class="btn btn-primary">サイトマップを見る</a>
    </p>
  </div><!-- / .page-content -->
</section><!-- / .no-results -->


</main><!-- / .site-main -->
</div><!-- / .content-area -->



<?php get_sidebar(); ?>



</div><!-- / .site-content-inner -->
</div><!-- / .site-content -->



<?php get_footer(); ?>
