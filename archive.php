<?php get_header(); ?>



<div id="content" class="site-content<?php if ( is_active_sidebar('sidebar-widget') ) { echo ' with-widget'; } ?>">
<div class="site-content-inner">



<div id="primary" class="content-area">
<main id="main" class="site-main">


  <?php if ( have_posts() ) : ?>
  <header class="page-header">
    <?php
      the_archive_title( '<h1 class="page-title h1">', '</h1>' );
      // the_archive_description( '<div class="archive-description">', '</div>' );
    ?>
  </header><!-- .page-header -->


  <div class="page-content">
  <?php
    if ( is_tax('タクソノミー名', 'ターム名') ) {
      get_template_part('template-parts/archive/content','archive-タクソノミー名-ターム名');
    } elseif ( is_post_type_archive('投稿タイプ名') ) {
      get_template_part('template-parts/archive/content','archive-投稿タイプ名');
    } else {
      get_template_part('template-parts/archive/content','archive');
    }
  ?>
  </div><!-- / .page-content -->


  <?php //wp_bootstrap_pagination(); ?>
  <?php else : ?>
  <?php get_template_part( 'template-parts/post/content', 'none' ); ?>
  <?php endif; ?>


  <div class="page-footer">
  </div><!-- / .page-footer -->


</main><!-- / .site-main -->
</div><!-- / .content-area -->



<?php get_sidebar(); ?>



</div><!-- / .site-content-inner -->
</div><!-- / .site-content -->



<?php get_footer(); ?>
